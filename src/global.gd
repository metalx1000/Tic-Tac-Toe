extends Node

var x_img = preload("res://assets/x.png")
var o_img = preload("res://assets/o.png")
var icons = [x_img,o_img]
var turn = 0

var gameover = false
onready var squares = get_tree().get_nodes_in_group("squares")

func check_winner():
	#check rows
	for i in range(0,2):
		for y in range(0,2):
			if squares[i+y].pvalue == squares[i+y+1].pvalue &&\
			squares[i+y+1].pvalue == squares[i+y+2].pvalue &&\
			squares[i+y].pvalue != null &&\
			squares[i+y+1].pvalue != null &&\
			squares[i+y+2].pvalue != null:
				done()
				return

	#check columns
	for i in range(0,1,2):
		for y in range(0,3):
			if squares[i+y].pvalue == squares[i+y+3].pvalue &&\
			squares[i+y+3].pvalue == squares[i+y+6].pvalue &&\
			squares[i+y].pvalue != null &&\
			squares[i+y+3].pvalue != null &&\
			squares[i+y+6].pvalue != null:
				done()
				return
	#check diagnally 
	if squares[0].pvalue == squares[4].pvalue &&\
	squares[4].pvalue == squares[8].pvalue &&\
	squares[0].pvalue != null &&\
	squares[4].pvalue != null &&\
	squares[8].pvalue != null:
		done()
		return
		
	if squares[2].pvalue == squares[4].pvalue &&\
	squares[4].pvalue == squares[6].pvalue &&\
	squares[2].pvalue != null &&\
	squares[4].pvalue != null &&\
	squares[6].pvalue != null:
		done()
		return
		
func done():
	gameover = true
	var msg = get_tree().get_nodes_in_group("msgs")[0]
	#var msg = get_tree().get_root().get_node("Label")
	
	msg.visible = true
	
func print_board():
	var line = "-------------"
	for x in range(0,3):
		line += "\n"
		for y in range(0,3):
			line += str(squares[x+y].pvalue)
			
	print(line)
