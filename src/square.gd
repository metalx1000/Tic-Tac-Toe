extends Button

var x_img = preload("res://assets/x.png")
var o_img = preload("res://assets/o.png")
var icons = [x_img,o_img]
var pvalue = null


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_square_pressed():
	if Global.gameover:
		return
	if pvalue == null:
		icon = icons[Global.turn]
		pvalue = Global.turn
		Global.turn += 1
		if Global.turn > 1:
			Global.turn = 0
		
		Global.check_winner()
